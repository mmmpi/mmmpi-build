#! /bin/bash
set -e

# Clone necessary layers (at the dylan branch where appropriate)
mkdir -p oe-mmmpi
git clone git://git.openembedded.org/bitbake.git \
	oe-mmmpi/bitbake

git clone git://git.openembedded.org/openembedded-core.git \
	oe-mmmpi/openembedded-core

git clone git://git.openembedded.org/meta-openembedded.git \
	oe-mmmpi/meta-openembedded

git clone git://git.yoctoproject.org/meta-yocto.git \
	oe-mmmpi/meta-yocto

git clone git://git.yoctoproject.org/meta-raspberrypi.git \
	oe-mmmpi/meta-raspberrypi

git clone https://bitbucket.org/homebrewtech/meta-mmmpi.git \
	oe-mmmpi/meta-mmmpi

# We need a symlink to bitbake within the openembedded-core directory
ln -s ../bitbake oe-mmmpi/openembedded-core/bitbake
